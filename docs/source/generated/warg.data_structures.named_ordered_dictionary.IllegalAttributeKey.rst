warg.data\_structures.named\_ordered\_dictionary.IllegalAttributeKey
====================================================================

.. currentmodule:: warg.data_structures.named_ordered_dictionary

.. autoexception:: IllegalAttributeKey