warg.arguments.ConfigObject
===========================

.. currentmodule:: warg.arguments

.. autoclass:: ConfigObject
   :members:
   :show-inheritance:
   :inherited-members:

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~ConfigObject.__init__
   
   

   
   
   